USE renewtoharmony
GO

-- SAMSPolicyNumbers change, clean them up as needed
UPDATE
  CasaClueOutput
SET
  CasaClueOutput.SAMSPolicyNo = tblPolicy.PolicyNo
FROM
  CasaClueOutput
JOIN
  tblProperty
ON
  tblProperty.IGD_ID = CasaClueOutput.igdid
JOIN
  tblPolicy
ON
  tblPolicy.PROPID = tblProperty.PROPID
GO

-- With Casa Clue data relinked, update records where RoofMaterial is missing, using data from Casa Clue Data
UPDATE
  tblProperty
SET
  tblProperty.RoofMaterial = CasaClueOutput.roofMaterial
FROM
  tblProperty
JOIN
  tblPolicy
ON
  tblPolicy.PROPID = tblProperty.PROPID
JOIN  
  CasaClueOutput
ON
  CasaClueOutput.SAMSPolicyNo = tblPolicy.PolicyNo
WHERE
  tblProperty.RoofMaterial IS NULL
GO

-- With Casa Clue data relinked, update records where replacement cost is missing, using data from Casa Clue
UPDATE
  tblProperty
SET
  tblProperty.ReplacementCost = CasaClueOutput.coverageLimits_dwelling_amount
FROM
  tblProperty
JOIN
  tblPolicy
ON
  tblPolicy.PROPID = tblProperty.PROPID
JOIN  
  CasaClueOutput
ON
  CasaClueOutput.SAMSPolicyNo = tblPolicy.PolicyNo
WHERE
  tblProperty.ReplacementCost IS NULL
AND
  CasaClueOutput.coverageLimits_dwelling_amount IS NOT NULL
GO

-- For UPC RI Records, 'Police' is equivalent to 'Central' for burglar Alarm
UPDATE
  tblHOPolicySupp
SET
  tblHOPolicySupp.TheftAlarm = 'Central'
FROM
  tblHOPolicySupp
JOIN
  tblPolicy
ON
  tblPolicy.SuppId = tblHOPolicySupp.SuppId
WHERE
  tblPolicy.zzCitizensTag like 'URH-%' -- UPC RI Identifier
AND
  tblHOPolicySupp.TheftAlarm = 'Police'
GO

-- For UPC RI Records, Premier = 2 is identical to Premier = 1
UPDATE
  tblHOPolicySupp
SET
  tblHOPolicySupp.Premier = 1
FROM
  tblHOPolicySupp
JOIN
  tblPolicy
ON
  tblPolicy.SuppId = tblHOPolicySupp.SuppId
WHERE
  tblPolicy.zzCitizensTag like 'URH-%' -- UPC RI Identifier
AND
  tblHOPolicySupp.Premier = 2
GO

-- FOR UPC RI Records, roofGeometry will always be 'OTHER'
UPDATE
  tblHOPolicySupp
SET
  tblHOPolicySupp.RoofShape = 'Other'
FROM
  tblHOPolicySupp
JOIN
  tblPolicy
ON
  tblPolicy.SuppId = tblHOPolicySupp.SuppId
WHERE
  tblPolicy.zzCitizensTag like 'URH-%' -- UPC RI Identifier
GO

-- Set Policy Holder Address 2s to strings where they are null
UPDATE
  tblPHolder
SET
  PHAddress2 = ''
WHERE
  PHAddress2 IS NULL
GO

-- Make RI DEDUCT match default expectations
UPDATE
  tblPolicy
SET
  DEDUCT = 1000
WHERE
  DEDUCT = 500
AND
  zzCitizensTag like 'URH-%'
GO

-- Make RI Medical Coverage match expected values
UPDATE
    tblPolicy
SET
    MEDPAYMENT = 5000
WHERE
    MEDPAYMENT IN (3000, 4000)
AND
    zzCitizensTag like 'URH-%'
GO